//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
{
texto: 'O céu não é o limite',
respostas: [
{ valor: 0, texto: 'Ter preguiça' },
{ valor: 1, texto: 'Quando der certo a gente faz' },
{ valor: 3, texto: 'Vamo pra cima familia' },
{ valor: 2, texto: 'Se der for viavel a gente faz' }
]
},
{
texto: 'Apaixonados pela missão',
respostas: [
{ valor: 0, texto: 'Projetos nada mais sao do que numeros' },
{ valor: 1, texto: 'Dinheiro é bom e é isto' },
{ valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
{ valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
]
},
{
texto: 'Ohana',
respostas: [
{ valor: 0, texto: 'Ta com problema? Se vira filhao' },
{ valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
{ valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
{ valor: 1, texto: 'Fora da Gti nem olho' }
]
},
{
texto: 'Integridade',
respostas: [
{ valor: 0, texto: 'Furar a fila do ru' },
{ valor: 2, texto: 'farol amarelo pra mim é verde' },
{ valor: 1, texto: 'Clonar cartao de credito' },
{ valor: 3, texto: 'Sou integro' }
]
},
{
texto: 'Ser capitão da nave',
respostas: [
{ valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
{ valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
{ valor: 1, texto: 'Ve la, depois me conta' },
{ valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
]
},
{texto: 'Você é o Brad',
respostas: [
{ valor: 1, texto: 'Quem é o Brad?' },
{ valor: 0, texto: 'Eu não sou o Brad' },
{ valor: 3, texto: 'Certamente eu sou o Brad' },
{ valor: 2, texto: 'Espera, talvez eu seja o Brad' }
]}
]
var soma = 0 //variável que somará os valores
var cont = -1 //variável que muda as perguntas
var i
function mostrarQuestao() {

var resp = document.getElementsByName('respostas') //variável que serve para acessar as respostas

if(resp[0] !== false || resp[1] !== false || resp[2] !== false || resp[3] !== false || cont == -1){ //Esse if serve pra verificar se pelos menos uma alternativa foi marcada
    if(cont != -1){ //Se for uma pergunta eu entro nesse if
        for(var j = 0; j < 5; j++){
            soma = soma + resp[j].checked*perguntas[cont]['respostas'][j]['valor'] //A soma feita para saber o valor final
        }
    }

    document.getElementById('resultado').innerHTML = "" //Deixa o resultado oculto
    document.getElementById('confirmar').innerHTML = 'Próxima' //Mudar o nome do botão
    cont++ //Incrementar o contador pra rodar as outras questões
    }

    if(cont <= 5){
        document.getElementById('titulo').innerHTML = perguntas[cont]['texto'] //Troca com o clique no botão para passar a próxima questão mudando o nome da questão
        document.getElementById('listaRespostas').style.display = 'inline' //Sair da página inicial e mostrar a lista das questões
        
        for(i = 0; i < 4; i++){ //Mudar os textos e os enunciados das questões
            resp[i].checked = false //Deixar todos os valores falsos pra não aparecer a resposta anterior
            resp[i].value = perguntas[cont]['respostas'][i]['valor'] //Deixar definido todos os valores dos itens
            document.getElementsByTagName('span')[i].innerHTML = perguntas[cont]['respostas'][i]['texto'] //Colocar os textos em cada item das questões
        }
    }
    else{
        finalizarQuiz(); //Chamar a função nomeada
    }
}
function finalizarQuiz() { //funçao pra finalizar ou resetar o quiz
document.getElementById("titulo").innerHTML = "RESULTADO DO QUIZ" //Mudar o título para o resultado do quiz
document.getElementById('listaRespostas').style.display = 'none' //Sumir com a lista dos botões
document.getElementById("resultado").innerHTML = "O seu resultado foi: " + parseInt((soma / 15)*100) + "%"
document.getElementById("confirmar").innerHTML = "TENTAR DE NOVO" //Mudar o nome do botão para refazer o quiz
cont = -1
soma = 0
}

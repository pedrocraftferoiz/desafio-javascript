var jogVez = 0; //Indicar o jogador da vez
var acabou = false; //Variável que vai indicar se o jogo acabou ou não

function selecionar(botao) { //Função que va indicar as jogadas
    if (acabou == false){ //Ver so jogo já terminou
        if (botao.innerHTML == "") { //Ver se a casa está preenchida
            var jog = document.getElementById("indicadorDaVez"); // Mostrar o jogador da vez, ou seja, "X" ou "O"
            if (jogVez % 2 == 0){
                botao.innerHTML = "X"; // Se a jogada da vez for numa vez par, o botão será o "X" a aparecer
                jogador.innerHTML = "O"; // Quem vai aparecer na próxima rodada
            }
            else {
                botao.innerHTML = "O"; // Se a jogada da vez for numa vez ímpar, o botão será o "O" a aparecer
                jogador.innerHTML = "X"; // Quem vai aparecer na próxima rodada
            }
            jogVez++; //Incrementar essa variável para mudar a vez do jogador
            ganhou(); //Função que mostrará as formas de ganhar ou não
        }
    }
}

// Zera todos as posições e recomeça o jogo
function resetar(){
    var jog = document.getElementById("indicadorDaVez"); // Mostrar o jogador da vez, ou seja, "X" ou "O"
    var vencedor = document.getElementById("indicadorVencedor"); //Indica quem vencer
    jog.style.display = "inline";
    document.getElementById("indicadorBase").style.display = "inline";
    jogVez = 0; // Reseta o contador das jogadas
    jog.innerHTML = "X"; // Começo o jogo sempre com "X"
    vencedor.innerHTML = null; // Tirar quem venceu
    terminado = false; // Recomeçao jogo
    for (var i = 0; i < 9; i++) { // For pra rodar pelas casas do jogo da velha
        document.getElementsByClassName("casa")[i].innerHTML = null; // Resetar todas as casas do jogo da velha
    }
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
    var vencedor = document.getElementById("indicadorVencedor"); // Mostra quem venceu
    var casa = document.getElementsByClassName("casa"); // Indica os quadrados do jogo da velha
    if (casa[0].innerHTML == "X" && casa[4].innerHTML == "X" && casa[8].innerHTML == "X") { //Possibilidade de ganhar
        vencedor.innerHTML = "O ganhador foi: X"; //O jogador que ganhou
        acabou = true; // Termina o jogo
    }
    if (casa[2].innerHTML == "X" && casa[4].innerHTML == "X" && casa[6].innerHTML == "X") { //Possibilidade de ganhar
        vencedor.innerHTML = "O ganhador foi: X"; //O jogador que ganhou
        acabou = true; // Termina o jogo
    }
    if (casa[0].innerHTML == "O" && casa[4].innerHTML == "O" && casa[8].innerHTML == "O") { //Possibilidade de ganhar
        vencedor.innerHTML = "O ganhador foi: O"; //O jogador que ganhou
        acabou = true; // Termina o jogo
    }
    if (casa[2].innerHTML == "O" && casa[4].innerHTML == "O" && casa[6].innerHTML == "O") { //Possibilidade de ganhar
        vencedor.innerHTML = "O ganhador foi: O"; //O jogador que ganhou
        acabou = true; // Termina o jogo
    }
    for (var j = 0; j <= 6; j = j + 3) {
        if (casa[j].innerHTML == "X" && casa[j + 1].innerHTML == "X" && casa[j + 2].innerHTML == "X") { //Possibilidade de ganhar
            vencedor.innerHTML = "O ganhador foi: X"; //O jogador que ganhou
            acabou = true; // Termina o jogo
            break;
        }
        else if (casa[j].innerHTML == "O" && casa[j + 1].innerHTML == "O" && casa[j + 2].innerHTML == "O") { //Possibilidade de ganhar
            vencedor.innerHTML = "O ganhador foi: O"; //O jogador que ganhou
            acabou = true; // Termina o jogo
            break;
        }
    }
    for (var j = 0; j < 3; j++) {
        if (casa[j].innerHTML == "X" && casa[j + 3].innerHTML == "X" && casa[j + 6].innerHTML == "X") { //Possibilidade de ganhar
            vencedor.innerHTML = "O ganhador foi: X"; //O jogador que ganhou
            acabou = true; // Termina o jogo
            break;
        }
        else if (casa[j].innerHTML == "O" && casa[j + 3].innerHTML == "O" && casa[j + 6].innerHTML == "O") { //Possibilidade de ganhar
            vencedor.innerHTML = "O ganhador foi: O"; //O jogador que ganhou
            acabou = true; // Termina o jogo
            break;
        }
    }
    if (jogVez > 8 && vencedor.innerHTML == "") { //Possibilidade de dar velha
        vencedor.innerHTML = "Deu velha"; // Mostrar que deu velha
        acabou = true; //termina o jogo
    }
    if (terminado == true){
        document.getElementById("indicadorDaVez").style.display = "none"; // Quando alguem ganhar, tirar a vez do jogador
        document.getElementById("indicadorBase").style.display = "none"; // Quando alguem ganhar, tirar a vez do jogador
    }
}
